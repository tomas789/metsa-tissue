﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Metsa_Tissue.Logs
{
    public class Logger
    {
        private Logger()
        {
        }

        private static Logger _instance;

        public static Logger GetInstance()
        {
            return _instance ?? (_instance = new Logger());
        }

        public LogCollection Collection { get; set; }

        public static void Log(Facility facility, string msg)
        {
            var instance = GetInstance();
            Application.Current.Dispatcher.BeginInvoke(
                (Action) (() => instance.Collection.Add(new LogItem {Message = msg, Facility = facility})));
        }

        public static void Log(Facility facility, string format, params object[] objects)
        {
            Log(facility, string.Format(format, objects));
        }

        public static void LogError(string msg)
        {
            Log(Facility.Error, msg);
        }

        public static void LogError(string format, params object[] objects)
        {
            Log(Facility.Error, format, objects);
        }

        public static void LogWarning(string msg)
        {
            Log(Facility.Warning, msg);
        }

        public static void LogWarning(string format, params object[] objects)
        {
            Log(Facility.Warning, format, objects);
        }

        public static void LogInfo(string msg)
        {
            Log(Facility.Info, msg);
        }

        public static void LogInfo(string format, params object[] objects)
        {
            Log(Facility.Info, format, objects);
        }

        public static void LogDebug(string msg)
        {
            Log(Facility.Debug, msg);
        }

        public static void LogDebug(string format, params object[] objects)
        {
            Log(Facility.Debug, format, objects);
        }

        public enum Facility
        {
            Debug, Info, Warning, Error
        }
    }
}
