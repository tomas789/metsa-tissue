﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metsa_Tissue.Converters;

namespace Metsa_Tissue.Logs
{
    public class LogItem : INotifyPropertyChanged
    {
        private DateTime _time = DateTime.Now;
        private Logger.Facility _facility;
        private string _message;

        public DateTime Time
        {
            get { return _time; }
            set
            {
                _time = value;
                NotifyPropertyChanged("Time");
            }
        }

        public Logger.Facility Facility
        {
            get { return _facility; }
            set
            {
                _facility = value;
                NotifyPropertyChanged("Facility");
            }
        }

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                NotifyPropertyChanged("Message");
            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1}: {2}", Facility, Time, Message);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
