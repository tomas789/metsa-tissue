﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Metsa_Tissue.Core;
using Metsa_Tissue.Logs;
using QuickGraph;

namespace Metsa_Tissue
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SimulationLoader _simulation;

        public static LogCollection LogCollection = new LogCollection();

        private string _solverName;

        public MainWindow()
        {
            InitializeComponent();
            _simulation = new SimulationLoader();

            Resources.Add("LogCollection", LogCollection);

            //LogBox.ItemsSource = LogCollection;
            var logInstance = Logger.GetInstance();
            logInstance.Collection = LogCollection;

            LoadResources();

            Logger.LogDebug("MainWindow constructor");
        }

        private void LoadExcel(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            var dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".xlsx",
                Filter = "Excel documents (.xlsx)|*.xlsx"
            };

            // Set filter for file extension and default file extension

            // Display OpenFileDialog by calling ShowDialog method
            var result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result != true)
                return;

            // Open document
            var filename = dlg.FileName;
            ExcelFileNameBox.Text = filename;

            _simulation.ExcelFileName = filename;

            InitializeButton.IsEnabled = filename.Length > 0;
        }

        private bool InputControlsEnabled
        {
            set
            {
                if (value)
                {
                    InitializeButton.IsEnabled = false;
                    BrowseExcelButton.IsEnabled = true;
                }
                else
                {
                    InitializeButton.IsEnabled = false;
                    BrowseExcelButton.IsEnabled = false;
                }

            }
        }

        private void LoadResources()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            _solverName = string.Format("{0}\\cs2.exe", appDataPath);

            Logger.LogDebug("Solver name: {0}", _solverName);

            if (File.Exists(_solverName))
                return;

            Logger.LogDebug("Loading cs2.exe resource to App Data path.");

            var assembly = Assembly.GetExecutingAssembly();
            var solverResourceStream = assembly.GetManifestResourceStream("Metsa_Tissue.cs2.exe");

            if (solverResourceStream == null)
                throw new InvalidOperationException("Failed loading inner resource cs2.exe");

            using (var solverStream = new FileStream(_solverName, FileMode.Create))
            {
                solverResourceStream.CopyTo(solverStream);
            }
        }

        private void InitializeSimulation(object sender, RoutedEventArgs e)
        {
            var useTryCatch = false;
            var t = new Task(async () =>
            {
                if (useTryCatch)
                {
                    try
                    {
                        _simulation.SolverExecutable = _solverName;
                        await _simulation.Initialize();
                    }
                    catch (Exception exception)
                    {
                        Logger.LogError(exception.ToString());
                    }
                    finally
                    {
                        Logger.LogInfo("END initialization.");
                    }
                }
                else
                {
                    _simulation.SolverExecutable = _solverName;
                    await _simulation.Initialize();
                }
            });
            t.Start();
        }
    }
}
