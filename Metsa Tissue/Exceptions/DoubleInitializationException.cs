﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metsa_Tissue.Exceptions
{
    class DoubleInitializationException : Exception
    { }
}
