using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Metsa_Tissue.Logs;

namespace Metsa_Tissue.Converters
{
    [ValueConversion(typeof(Logger.Facility), typeof(Colors))]
    public class FacilityToColorsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var facility = (Logger.Facility)value;

            switch (facility)
            {
                case Logger.Facility.Debug:
                    return Colors.Transparent;
                case Logger.Facility.Info:
                    return Colors.SkyBlue;
                case Logger.Facility.Warning:
                    return Colors.Yellow;
                case Logger.Facility.Error:
                    return Colors.Red;
                default:
                    throw new Exception("Impossible FacilityConverter");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}