﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Annotations;
using System.Windows.Shapes;
using GraphSharp.Algorithms.Layout;
using Metsa_Tissue.Logs;
using QuickGraph;
using QuickGraph.Algorithms.RandomWalks;
using QuickGraph.Graphviz;
using QuickGraph.Graphviz.Dot;

namespace Metsa_Tissue.Core
{
    class Simulation
    {
        private SimulationNode _sourceNode;
        private SimulationNode _unavailableSourceNode;

        public AdjacencyGraph<SimulationNode, TaggedEdge<SimulationNode, EdgeTag>> Graph { get; private set; }

        private BiDictionaryOneToOne<SimulationNode, int> _nodeNumbers; 

        public bool WasBuild { get; private set; }

        private readonly Dictionary<SimulationNode, long> _supplyMap = new Dictionary<SimulationNode, long>();

        public string OutputPath;

        public bool AllowInternalRedistribution { get; set; }

        private List<PaperMill> _paperMills;
        private List<LinePG> _linePgs;
        private List<WarehousePG> _warehousePgs;
        private List<CustomerPG> _customerPgs;

        public long? Flow { get; private set; }

        public void BuildGraph(List<PaperMill> paperMills, List<LinePG> linePgs, List<WarehousePG> warehousePgs,
            List<CustomerPG> customerPgs)
        {
            if (WasBuild)
                throw new InvalidOperationException("Building graph which was already build.");
            WasBuild = true;

            _paperMills = paperMills ?? new List<PaperMill>();
            _linePgs = linePgs ?? new List<LinePG>();
            _warehousePgs = warehousePgs ?? new List<WarehousePG>();
            _customerPgs = customerPgs ?? new List<CustomerPG>();

            LoadPrerequisities();

            LoadNodes();
            LoadArcs();
            LoadSupplyDemand();
        }

        private void LoadNodes()
        {
            _sourceNode.AddVertex(Graph);
            _unavailableSourceNode.AddVertex(Graph);

            foreach (var node in _paperMills)
                node.AddVertex(Graph);

            foreach (var node in _linePgs)
                node.AddVertex(Graph);

            foreach (var node in _warehousePgs)
                node.AddVertex(Graph);

            foreach (var node in _customerPgs)
                node.AddVertex(Graph);
        }

        private void LoadArcs()
        {
            _unavailableSourceNode.AddSource(Graph, _sourceNode);

            var basepaperWarehouses = (from wh in _warehousePgs where wh.Pg.IsBasepaper select wh).ToArray();
            var finishedGoodsWarehouses = (from wh in _warehousePgs where !wh.Pg.IsBasepaper select wh).ToArray();

            /* FLOW: From source to every paper mill */
            foreach (var paperMill in _paperMills)
                paperMill.AddSource(Graph, _sourceNode);

            /* FLOW: From paper mill to basepaper warehouse */
            foreach (var bpWh in basepaperWarehouses)
                foreach (var paperMill in _paperMills)
                    bpWh.AddSource(Graph, paperMill);

            /* FLOW: From basepaper warehouse to lines */
            foreach (var linePg in _linePgs)
                foreach (var wh in basepaperWarehouses)
                    linePg.AddSource(Graph, wh);

            /* FLOW: From line to warehouse */
            foreach (var linePg in _linePgs)
            {
                var pg = linePg;
                foreach (var fgWh in finishedGoodsWarehouses.Where((x) => x.Pg.Equals(pg.Pg)))
                    fgWh.AddSource(Graph, linePg);
            }

            /* FLOW: From unavailableSource to unavailable product group warehouses */
            var availablePgs = (from linePg in _linePgs select linePg.Pg);
            var requiredPgs = new HashSet<ProductGroup>(from customerPg in _customerPgs select customerPg.Pg);
            requiredPgs.ExceptWith(availablePgs);
            foreach (var pg in requiredPgs)
            {
                var pgLocal = pg;
                var warehouses = from wh in _warehousePgs where wh.Pg.Equals(pgLocal) select wh;
                foreach (var wh in warehouses)
                    wh.AddSource(Graph, _unavailableSourceNode);
            }

            /* REDISTRIBUTION */
            foreach (var from in basepaperWarehouses)
                foreach (var to in basepaperWarehouses)
                    to.AddSource(Graph, from);

            /* REDISTRIBUTION */
            var productGroups =
                (from wh in _warehousePgs where !wh.Pg.IsBasepaper select wh.Pg).Distinct(new ProductGroup.Comparer());
            foreach (var pg in productGroups)
            {
                var pgLocal = pg;
                var feasibleWarehouses = (_warehousePgs.Where(x => x.Pg.Equals(pgLocal))).ToArray();
                foreach (var from in feasibleWarehouses)
                    foreach (var to in feasibleWarehouses)
                        to.AddSource(Graph, @from);
            }
        }

        private void LoadSupplyDemand()
        {
            _supplyMap.Add(_sourceNode, 0);

            /* Customer demand */
            foreach (var customerPg in _customerPgs)
            {
                SimulationNode closestFeasible = null;
                var pg = customerPg;
                var feasibles = (from wh in _warehousePgs where wh.Pg.Equals(pg.Pg) select wh).ToArray();

                foreach (var feasible in feasibles)
                {
                    if (closestFeasible == null)
                        closestFeasible = feasible;
                    else
                        if (feasible.TransportationCost(customerPg) < closestFeasible.TransportationCost(customerPg))
                            closestFeasible = feasible;
                }

                if (closestFeasible == null)
                    closestFeasible = _unavailableSourceNode;

                customerPg.AddSource(Graph, closestFeasible);

                if (!_supplyMap.ContainsKey(closestFeasible))
                    _supplyMap.Add(closestFeasible, 0);

                _supplyMap[closestFeasible] -= customerPg.Demand;
                _supplyMap[_sourceNode] += customerPg.Demand;
            }
        }

        private void LoadPrerequisities()
        {
            _sourceNode = new Source(this);
            _unavailableSourceNode = new UnavailableSource(this);
            Graph = new AdjacencyGraph<SimulationNode, TaggedEdge<SimulationNode, EdgeTag>>();
        }

        public void LoadFlow(LinqToExcel.Query.ExcelQueryable<LinqToExcel.Row> rows, out List<string> debug)
        {
            var lineNumber = 1;
            debug = new List<string>();
            foreach (var row in rows)
            {
                ++lineNumber;

                if (row["KG"] == "0")
                    continue;

                string storage = row["Storage ID"];
                var pg = new ProductGroup(row["Quality_corr"] != "FG" ? "Basepaper" : row["Material"]);


                var feasibleWarehouses = (from wh in _warehousePgs where /* wh.Storage == storage && */ wh.Pg.Equals(pg) select wh).ToArray();
                if (feasibleWarehouses.Length != 1)
                {
                    debug.Add(string.Format("Skipping line {0} because {1} feasible warehouses found.", lineNumber, feasibleWarehouses.Length));
                    continue;
                }

                TaggedEdge<SimulationNode, EdgeTag> feasibleEdge = null;
                foreach (var edge in Graph.OutEdges(feasibleWarehouses.First()))
                {
                    if (pg.IsBasepaper)
                    {
                        if (edge.Target.GetType() != typeof (WarehousePG))
                            continue;

                        var wh = (WarehousePG) edge.Target;
                        if (!wh.Pg.Equals(pg) || wh.City != row["City of Ship-To Part"])
                            continue;
                    }
                    else
                    {
                        if (edge.Target.GetType() != typeof(CustomerPG))
                            continue;

                        var cpg = (CustomerPG) edge.Target;

                        if (!cpg.Pg.Equals(pg) || cpg.City != row["City of Ship-To Part"])
                            continue;
                    }

                    feasibleEdge = edge;
                    break;
                }

                if (feasibleEdge == null)
                {
                    debug.Add(string.Format("Skipping line {0} because no target found.", lineNumber));
                    continue;
                }

                long increment;
                if (!long.TryParse(row["KG"], out increment))
                {
                    debug.Add(string.Format("Skipping line {0} because parsing kilos failed ({1}).", lineNumber, row["KG"]));
                    continue;
                }

                feasibleEdge.Tag.LoadedFlow += increment;
            }
        }

        public string GenerateDOT()
        {
            Logger.LogInfo("BEGIN generating DOT");

            if (!WasBuild)
                throw new InvalidOperationException("Generating DOT on uninitialized graph.");

            var gw = new GraphvizAlgorithm<SimulationNode, TaggedEdge<SimulationNode, EdgeTag>>(Graph);
            gw.FormatVertex += gw_FormatVertex;
            gw.FormatEdge += gw_FormatEdge;
            var output = gw.Generate(new DotWriter(), "Metsa Tissue");

            Logger.LogInfo("END generating DOT");

            return output;
        }

        public string GenerateBasepaperDot()
        {
            var bpGraph = new AdjacencyGraph<SimulationNode, TaggedEdge<SimulationNode, EdgeTag>>();

            bpGraph.AddVertex(_sourceNode);
            foreach (var paperMill in _paperMills)
                bpGraph.AddVertex(paperMill);
            foreach (var warehousePg in _warehousePgs.Where(n => n.Pg.IsBasepaper))
                bpGraph.AddVertex(warehousePg);

            foreach (var node in bpGraph.Vertices)
                foreach (var edge in Graph.OutEdges(node))
                    if (bpGraph.Vertices.Contains(edge.Target))
                        bpGraph.AddEdge(edge);

            var gw = new GraphvizAlgorithm<SimulationNode, TaggedEdge<SimulationNode, EdgeTag>>(bpGraph);
            gw.FormatVertex += gw_FormatVertex;
            gw.FormatEdge += gw_FormatEdge;
            return gw.Generate(new DotWriter(), "Metsa Tissue");
        }

        public IList<string> GenerateOutputToSolver()
        {
            var lines = new List<string>();

            lines.Add(string.Format("p min {0} {1}", Graph.VertexCount, Graph.EdgeCount));

            /* TODO: Node supply */

            var i = 0;
            _nodeNumbers = Graph.Vertices.ToBiDictionary((n) => n, (n) => ++i);

            var lesserThenToneCount = 0;
            var totalCount = 0;

            foreach (var nodeItem in _supplyMap)
            {
                var node = nodeItem.Key;
                var supplyDemand = nodeItem.Value;

                if (supplyDemand == 0)
                {
                    Logger.LogDebug("IMPOSSIBLE: GOT ZERO SUPPLY/DEMAND");
                    continue;
                }

                if (supplyDemand < 0 && supplyDemand > -1000)
                    ++lesserThenToneCount;
                ++totalCount;

                var nn = _nodeNumbers.GetByFirst(node);
                var line = string.Format("n {0} {1}", nn, supplyDemand);
                lines.Add(line);
            }

            Logger.LogInfo("{0}/{1} lesser then tone", lesserThenToneCount, totalCount);

            lines.AddRange(from arc in Graph.Edges
                           let capacity = arc.Tag.Capacity
                           let cost = false ? 1 : (long) Math.Round(arc.Tag.Cost/1000)
                           let snn = _nodeNumbers.GetByFirst(arc.Source)
                           let tnn = _nodeNumbers.GetByFirst(arc.Target)
                           select string.Format("a {0} {1} {2} {3} {4}", snn, tnn, 0, capacity, cost));

            return lines;
        }

        public void LoadSolution(IList<string> solverLines)
        {
            foreach (var line in solverLines)
            {
                /* skip comment */
                if (string.IsNullOrEmpty(line) || line[0] == 'c')
                    continue;

                var parts = line.Split(new[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);

                switch (line[0])
                {
                    case 's':
                        // TODO: Do checks here
                        Flow = long.Parse(parts[1]);
                        break;
                    case 'f':
                        var srcNode = _nodeNumbers.GetBySecond(int.Parse(parts[1]));
                        var dstNode = _nodeNumbers.GetBySecond(int.Parse(parts[2]));
                        var flow = long.Parse(parts[3]);

                        var edge = Graph.OutEdges(srcNode).FirstOrDefault(e => e.Target == dstNode);
                        if (edge == null)
                            throw new InvalidOperationException("Unable to find edge from solver output!");

                        edge.Tag.Flow = flow;
                        break;
                }
            }
        }

        private readonly List<string> _prepairedOutput = new List<string>(); 

        public List<string> GenerateOutput()
        {
            var output = _prepairedOutput;
            foreach (var edge in Graph.Edges)
            {
                bool isFlowToCustomer = edge.Target.GetType() == typeof (CustomerPG);
                bool isZeroFlow = edge.Tag.Flow == 0;
                bool isSourceOrTargetInternal = edge.Source.IsInternal() || edge.Target.IsInternal();

                if (!isFlowToCustomer && (isZeroFlow || isSourceOrTargetInternal))
                    continue;

                var lineItem =
                    edge.Source.Describe()
                        .Concat(edge.Target.Describe())
                        .Concat(new[] {isFlowToCustomer ? ((CustomerPG)edge.Target).Demand.ToString() : edge.Tag.Flow.ToString(), edge.Source.TransportationCost(edge.Target).ToString()});

                output.Add(string.Join(";", lineItem));
            }

            return output;
        } 

        class DotWriter : IDotEngine
        {
            public string Run(GraphvizImageType imageType, string dot, string outputFileName)
            {
                return dot;
            }
        }

        public void gw_FormatVertex(object sender, FormatVertexEventArgs<SimulationNode> e)
        {
            var v = e.Vertex;
            e.VertexFormatter.Label = String.Join(" ", v.Describe());
        }

        public void gw_FormatEdge(object sender, FormatEdgeEventArgs<SimulationNode, TaggedEdge<SimulationNode, EdgeTag>> e)
        {
            var edge = e.Edge;
            e.EdgeFormatter.Label = new GraphvizEdgeLabel { Value = edge.Describe() };
        }
    }


}
