﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Metsa_Tissue.Core
{
    class CustomerPG : SimulationNode
    {
        private long _kg;
        private long _tu;
        private long _du;
        private long _lu;

        private string _postalCode;
        private string _city;
        private string _country;

        public string City { get { return _city; } }

        public long Demand
        {
            get { return _kg; }
        }

        public ProductGroup Pg { get; private set; }

        public CustomerPG(int lineNumber, double latitude, double longitude, ProductGroup pg, long kg, long tu, long du, long lu)
        {
            LineNumber = lineNumber;
            Latitude = latitude;
            Longitude = longitude;
            Pg = pg;
            _kg = kg;
            _tu = tu;
            _du = du;
            _lu = lu;
        }

        public override string DescribeNodeType()
        {
            return "Customer/PG";
        }

        public override string DescribeLocation()
        {
            return string.Format("Country: {0} City: {1} Postal: {2}", _country, _city, _postalCode);
        }

        public static CustomerPG LoadRow(int lineNumber, LinqToExcel.Row row, CultureInfo ci)
        {
            double latitude, longitude;
            long kg, lu, tu, du;

            if (!double.TryParse(row["Latitude"], NumberStyles.Float, ci, out latitude))
                return null;

            if (!double.TryParse(row["Longitude"], NumberStyles.Float, ci, out longitude))
                return null;

            var pg = new ProductGroup(row["Quality_BP"] == "FG" ? row["Material"] : "Basepaper");

            if (!long.TryParse(row["Součet z KG"], NumberStyles.None, ci, out kg))
                return null;

            if (!long.TryParse(row["Součet z TU"], NumberStyles.None, ci, out tu))
                return null;

            if (!long.TryParse(row["Součet z DU"], NumberStyles.None, ci, out du))
                return null;

            if (!long.TryParse(row["Součet z LU"], NumberStyles.None, ci, out lu))
                return null;

            try
            {
                return new CustomerPG(lineNumber, latitude, longitude, pg, kg, tu, du, lu)
                {
                    _postalCode = row["Postal Code"],
                    _country = row["Country"],
                    _city = row["City of Ship-To Part"]
                };
            }
            catch
            {
                return null;
            }
        }
    }
}
