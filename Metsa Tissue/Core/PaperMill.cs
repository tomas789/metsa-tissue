﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metsa_Tissue.Core
{
    class PaperMill : SimulationNode
    {
        private string _city;
        public PaperMill(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public PaperMill(double latitude, double longitude, string city)
        {
            Latitude = latitude;
            Longitude = longitude;
            _city = city;
        }

        public override string DescribeNodeType()
        {
            return "Paper Mill";
        }

        public override string DescribeLocation()
        {
            return string.Format("City: {0}", _city);
        }

        public static PaperMill LoadRow(LinqToExcel.Row row, CultureInfo ci)
        {
            double latitude, longitude;

            if (!double.TryParse(row["Latitude"], NumberStyles.Float, ci, out latitude))
                throw new Exception(string.Format("Unable to parse as double {0}", row["Latitude"]));

            if (!double.TryParse(row["Longitude"], NumberStyles.Float, ci, out longitude))
                throw new Exception(string.Format("Unable to parse as double {0}", row["Longitude"]));

            return new PaperMill(latitude, longitude, row["City"]);
        }
    }
}
