﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metsa_Tissue.Core
{
    class ExternalSupplier : SimulationNode
    {
        public ProductGroup Pg { get; private set; }

        public ExternalSupplier(ProductGroup pg)
        {
            Pg = pg;
        }

        public override bool IsInternal()
        {
            return true;
        }
    }
}
