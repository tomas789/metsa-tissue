﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using Metsa_Tissue.Logs;

namespace Metsa_Tissue.Core
{
    class WarehousePG : SimulationNode
    {
        public ProductGroup Pg { get; private set; }

        private string _storage;
        private string _city;
        private string _internal;

        public string Storage { get { return _storage; } }
        public string City { get { return _city; } }

        public WarehousePG(int lineNumber, double latitude, double longitude, ProductGroup pg)
        {
            Latitude = latitude;
            Longitude = longitude;
            Pg = pg;
            LineNumber = lineNumber;
        }

        public override string DescribeNodeType()
        {
            return "Warehouse/PG";
        }

        public override string DescribeLocation()
        {
            return string.Format("External: {0} City: {1}, Storage: {2}", _internal, _city, _storage);
        }

        public static WarehousePG LoadRow(int lineNumber, LinqToExcel.Row row, CultureInfo ci)
        {
            double latitude, longitude;

            if (!double.TryParse(row["Latitude"], NumberStyles.Float, ci, out latitude))
                throw new Exception(string.Format("Unable to parse as double {0}", row["Latitude"]));

            if (!double.TryParse(row["Longitude"], NumberStyles.Float, ci, out longitude))
                throw new Exception(string.Format("Unable to parse as double {0}", row["Longitude"]));

            var pg = new ProductGroup(row["Quality_BP"] == "FG" ? row["Material"] : "Basepaper");

            return new WarehousePG(lineNumber, latitude, longitude, pg)
            {
                _storage = row["Storage"],
                _city = row["City"],
                _internal = row["Internal/External"]
            };
        }
    }
}
