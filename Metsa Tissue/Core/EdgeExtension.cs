﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickGraph;

namespace Metsa_Tissue.Core
{
    static class EdgeExtension
    {
        public static string Describe(this TaggedEdge<SimulationNode, EdgeTag> edge)
        {
            return string.Format("{0}/{1},{2}", edge.Tag.Flow, edge.Tag.Capacity, edge.Tag.Cost);
        }
    }
}
