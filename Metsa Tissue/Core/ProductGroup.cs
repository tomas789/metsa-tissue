﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metsa_Tissue.Core
{
    class ProductGroup : IEquatable<ProductGroup>
    {
        public string Name { get; private set; }
        public ProductGroup(string name)
        {
            Name = name;
        }

        public bool IsBasepaper
        {
            get { return Name == "Basepaper" || Name == "basepaper"; }
        }

        public bool Equals(ProductGroup other)
        {
            return Name == other.Name;
        }

        public class Comparer : EqualityComparer<ProductGroup>
        {
            public override bool Equals(ProductGroup x, ProductGroup y)
            {
                return x.Equals(y);
            }

            public override int GetHashCode(ProductGroup obj)
            {
                return (from c in obj.Name select (int) c).Sum();
            }
        }
    }
}
