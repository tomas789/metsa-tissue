﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metsa_Tissue.Core
{
    class LinePG : SimulationNode
    {
        public ProductGroup Pg { get; private set; }

        private string _line;
        private string _city;
        private string _location;

        public LinePG(double latitude, double longitude, ProductGroup pg)
        {
            Latitude = latitude;
            Longitude = longitude;
            Pg = pg;
        }

        public override string DescribeNodeType()
        {
            return "Line/PG";
        }

        public override string DescribeLocation()
        {
            return string.Format("Location: {0}", _location);
        }

        public static LinePG LoadRow(LinqToExcel.Row row, CultureInfo ci)
        {
            double latitude, longitude;

            if (!double.TryParse(row["Latitude"], NumberStyles.Float, ci, out latitude))
                throw new Exception(string.Format("Unable to parse as double {0}", row["Latitude"]));

            if (!double.TryParse(row["Longitude"], NumberStyles.Float, ci, out longitude))
                throw new Exception(string.Format("Unable to parse as double {0}", row["Longitude"]));

            var pg = new ProductGroup(row["Material"]);

            return new LinePG(latitude, longitude, pg)
            {
                _city = row["City"],
                _line = row["Linka"],
                _location = row["Location"]
            };
        }
    }
}
