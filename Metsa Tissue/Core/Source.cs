﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metsa_Tissue.Core
{
    internal class Source : SimulationNode
    {
        private Simulation _simulation;

        public Source(Simulation simulation)
        {
            _simulation = simulation;
        }

        public override string DescribeNodeType()
        {
            return "Source (internal)";
        }

        public override bool IsInternal()
        {
            return true;
        }
    }
}
