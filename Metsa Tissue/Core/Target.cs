﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metsa_Tissue.Core
{
    class Target : SimulationNode
    {
        private Simulation _simulation;

        public Target(Simulation simulation)
        {
            _simulation = simulation;
        }

        public override string DescribeNodeType()
        {
            return "Target (internal)";
        }

        public override bool IsInternal()
        {
            return true;
        }
    }
}
