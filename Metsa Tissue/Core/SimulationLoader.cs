﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Windows.Shapes;
using LinqToExcel;
using Metsa_Tissue.Exceptions;
using Metsa_Tissue.Logs;
using Path = System.IO.Path;

namespace Metsa_Tissue.Core
{
    class SimulationLoader
    {
        private CultureInfo _ci = CultureInfo.CurrentCulture;

        private bool _initialized;

        private string _excelFileName;

        private ExcelQueryFactory _excel;

        private string _solverExecutable;

        public string SolverExecutable
        {
            get { return _solverExecutable; }
            set
            {
                if (_initialized)
                    throw new ChangingStateAfterInitialization();
                _solverExecutable = value;
            }
        }

        public string ExcelFileName
        {
            get { return _excelFileName; }
            set
            {
                if (_initialized)
                    throw new ChangingStateAfterInitialization();

                _excelFileName = value;
                Logger.LogDebug("ExcelFileName changed");
            }
        }

        public async Task Initialize()
        {
            if (_initialized)
                throw new DoubleInitializationException();

            _excel = new ExcelQueryFactory(ExcelFileName);

            await LoadDataAsync();
            //await LoadDemoAsync();

            _initialized = true;

            Debug.WriteLine("DoNe");

            Logger.LogInfo("Simulation initialized.");
        }

        private async Task LoadDemoAsync()
        {
            Logger.LogInfo("BEGIN loading demo application.");

            var paperMills = new List<PaperMill>
            {
                new PaperMill(52, 18, "City 1"),
                new PaperMill(53, 19, "City 2")
            };

            /*
            var linePgs = new List<LinePG>
            {
                new LinePG(52.5, 18.5, new ProductGroup("Product Group 1"))
            };
             */

            /*
            var warehousePgs = new List<WarehousePG>
            {
                new WarehousePG(55, 20, new ProductGroup("Product Group 1"))
            };
             */

            /*
            var customerPgs = new List<CustomerPG>
            {
                new CustomerPG(56, 21, new ProductGroup("Product Group 1"), 200, 200, 200, 200)
            };
             */

            var customerPgs = (await LoadCustomerPgsFromExcelAsync()).Take(1000).ToList();

            var pgList = (from cpg in customerPgs select cpg.Pg).Distinct(new ProductGroup.Comparer());
            var linePgs = (from pg in pgList select new LinePG(52.5, 18.5, pg)).ToList();
            var warehousePgs = (from pg in pgList select new WarehousePG(-1, 55, 20, pg)).ToList();

            var simulation = new Simulation();
            simulation.BuildGraph(paperMills, linePgs, warehousePgs, customerPgs);

            //var dot = simulation.GenerateDOT();
            //GenerateGraphToPngUsingExternalGraphviz(dot);

            List<string> debugFromFlows;
            simulation.LoadFlow(_excel.Worksheet("Toky"), out debugFromFlows);

            await SolveUsingExternal(simulation);
            Logger.LogInfo("SOLVER DONE: Flow: {0}", simulation.Flow.HasValue ? simulation.Flow.ToString() : "NULL");

            Logger.LogInfo("END loading demo application.");
        }

        private async Task LoadDataAsync()
        {
            Logger.LogInfo("Begin loading excel-based application.");

            var outputPath = Path.GetDirectoryName(_excelFileName);

            var customerPgsTask = LoadCustomerPgsFromExcelAsync();
            var linePgsTask = LoadLinePgsFromExcelAsync();
            var paperMillsTask = LoadPaperMillsFromExcelAsync();
            var warehousePgsTask = LoadWarehousePgsFromExcelAsync();

            Logger.LogDebug("BEGIN Loading from Excel");

            var paperMills = await paperMillsTask;
            var linePgs = await linePgsTask;
            var warehousePgs = await warehousePgsTask;
            var customerPgs = await customerPgsTask;

            Logger.LogDebug("END Loading from Excel");

            Logger.LogDebug("BEGIN Building graph");

            var simulation = new Simulation
            {
                OutputPath = outputPath
            };

            simulation.BuildGraph(paperMills.ToList(), linePgs.ToList(), warehousePgs.ToList(), customerPgs.ToList());

            Logger.LogDebug("END Building graph");

            Logger.LogDebug("BEGIN Solver");

            List<string> debugFromFlows;
            simulation.LoadFlow(_excel.Worksheet("Toky"), out debugFromFlows);

            File.WriteAllLines(outputPath + @"\FlowLoadDebug.txt", debugFromFlows);

            var bpDot = simulation.GenerateBasepaperDot();
            File.WriteAllText(outputPath + @"\BpGraph.dot.txt", bpDot);

            Logger.LogInfo("SOLVER DONE: Flow: {0}", simulation.Flow.HasValue ? simulation.Flow.ToString() : "NULL");

            Logger.LogInfo("END Loading excel-based application.");
        }

        private async Task SolveUsingExternal(Simulation simulation, TextWriter solverInputMirror = null, TextWriter solverOutputMirror = null)
        {
            if (simulation == null)
                throw new ArgumentNullException("simulation", "Trying to run solver at null object");

            var solverInput = simulation.GenerateOutputToSolver();

            Logger.LogDebug("DONE generating graph.");

            /*
            Task solverInputMirrorTask = null;
            if (solverInputMirror != null)
                solverInputMirrorTask = solverInputMirror.WriteLineAsync(solverInput);
             */

            var p = new Process
            {
                StartInfo =
                {
                    FileName = _solverExecutable,
                    UseShellExecute = false,
                    RedirectStandardInput = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true
                }
            };

            p.Start();

            var inputFile = string.Format(@"{0}\MetsaSolverInput.txt",
                Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
            File.WriteAllLines(inputFile, solverInput);

            foreach (var line in solverInput)
                p.StandardInput.WriteLine(line);

            p.StandardInput.Close();

            var solverOutputLines = new List<string>();

            while (true)
            {
                if (p.WaitForExit(2000))
                {
                    Logger.LogInfo("DONE solving!");
                    break;
                }

                while (!p.StandardOutput.EndOfStream)
                    solverOutputLines.Add(p.StandardOutput.ReadLine());

                while (!p.StandardError.EndOfStream)
                    Logger.LogError("SOLVER ERROR: {0}", p.StandardError.ReadLine());
                
                Logger.LogInfo("Still solving ...");
            }

            simulation.LoadSolution(solverOutputLines);

            var solution = simulation.GenerateOutput();
            var solutionFile = string.Format(@"{0}\MetsaSolution.txt",
                Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
            File.WriteAllLines(solutionFile, solution);

            p.Close();
        }

        private void GenerateGraphToPngUsingExternalGraphviz(string dot)
        {
            var workDir = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            File.WriteAllText(workDir + @"\metsa.dot", dot);

            var p = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    FileName = @"C:\Program Files (x86)\Graphviz 2.28\bin\dot.exe",
                    Arguments = string.Format(@"{0}\metsa.dot -o {0}\metsa.png -Tpng", workDir)
                }
            };

            // p.Start();
            // var output = p.StandardOutput.ReadToEnd();
            //p.WaitForExit();

            //Logger.LogInfo("Done DOT ({0}) {1}", p.ExitCode, output);
        }

        private async Task<IEnumerable<PaperMill>> LoadPaperMillsFromExcelAsync()
        {
            var paperMills = from pm in _excel.Worksheet("PaperMill").AsParallel() select PaperMill.LoadRow(pm, _ci);
            var skipped = (from pm in paperMills where pm == null select pm).Count();

            if (skipped == 0)
                return paperMills;

            Logger.LogWarning("Skipped {0} Paper mills", skipped);
            return paperMills.Where(x => x != null);
        }

        private async Task<IEnumerable<CustomerPG>> LoadCustomerPgsFromExcelAsync()
        {
            var lineNumber = 1 + 1;
            var skipped = 0;
            var customerPgs = new List<CustomerPG>();
            foreach (var row in _excel.Worksheet("Customers"))
            {
                var wh = CustomerPG.LoadRow(lineNumber++, row, _ci);
                if (wh == null)
                {
                    ++skipped;
                    continue;
                }

                customerPgs.Add(wh);
            }

            if (skipped != 0)
                Logger.LogWarning("Skipped {0} Customer / Product groups.", skipped);

            return customerPgs;
        }

        private async Task<IEnumerable<LinePG>> LoadLinePgsFromExcelAsync()
        {
            var linePgs = from lpg in _excel.Worksheet("Converting").AsParallel() select LinePG.LoadRow(lpg, _ci);
            var skipped = (from pm in linePgs where pm == null select pm).Count();

            if (skipped == 0)
                return linePgs;

            Logger.LogWarning("Skipped {0} Line / Product groups", skipped);
            return linePgs.Where(x => x != null);
        }

        private async Task<IEnumerable<WarehousePG>> LoadWarehousePgsFromExcelAsync()
        {
            var lineNumber = 1 + 1;
            var skipped = 0;
            var warehousePgs = new List<WarehousePG>();
            foreach (var row in _excel.Worksheet("Warehouse"))
            {
                var wh = WarehousePG.LoadRow(lineNumber++, row, _ci);
                if (wh == null)
                {
                    ++skipped;
                    continue;
                }

                warehousePgs.Add(wh);
            }

            if (skipped != 0)
                Logger.LogWarning("Skipped {0} Warehouse / Product groups.", skipped);

            return warehousePgs;
        }
    }
}
