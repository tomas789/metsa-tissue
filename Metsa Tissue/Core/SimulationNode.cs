﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickGraph;

namespace Metsa_Tissue.Core
{
    public abstract class SimulationNode
    {
        private double _latitude;
        private double _longitude;

        protected double Latitude
        {
            get { return _latitude; }
            set
            {
                if (value < -90 || value > 90)
                    throw new ArgumentOutOfRangeException("value", "Latitude must be between -90 and +90.");
                _latitude = value;
            }
        }

        protected double Longitude
        {
            get { return _longitude; }
            set
            {
                if (value < -180 || value > 180)
                    throw new ArgumentOutOfRangeException("value", "Longitude must be between -180 and 180.");
                _longitude = value;
            }
        }

        public int LineNumber { get; set; }

        public virtual bool IsInternal()
        {
            return false;
        }

        public virtual string DescribeNodeType()
        {
            return "SimulationNode";
        }

        public virtual string DescribeLocation()
        {
            return "unknown";
        }

        public virtual List<string> Describe()
        {
            return new List<string>
            {
                DescribeNodeType(),
                DescribeLocation()
            };
        }

        public virtual long TransportationCost(SimulationNode other)
        {
            if (IsInternal() || other.IsInternal())
                return 0;

            if (Longitude < -180 || Longitude > 180)
                throw new Exception();

            if (other.Longitude < -180 || other.Longitude > 180)
                throw new Exception();


            var a = new GeoCoordinate(Latitude, Longitude);
            var b = new GeoCoordinate(other.Latitude, other.Longitude);
            return (long)a.GetDistanceTo(b);
        }

        private List<TaggedEdge<SimulationNode, EdgeTag>> _inEdges = new List<TaggedEdge<SimulationNode, EdgeTag>>();
        private List<TaggedEdge<SimulationNode, EdgeTag>> _outEdges = new List<TaggedEdge<SimulationNode, EdgeTag>>();

        public int InDegree { get { return _inEdges.Count; } }
        public int OutDegree { get { return _outEdges.Count; } }

        public List<TaggedEdge<SimulationNode, EdgeTag>> InEdges { get { return _inEdges; } }
        public List<TaggedEdge<SimulationNode, EdgeTag>> OutEdges { get { return _outEdges; } }

        public virtual void AddVertex(AdjacencyGraph<SimulationNode, TaggedEdge<SimulationNode, EdgeTag>> graph)
        {
            graph.AddVertex(this);
        }

        public virtual void AddSource(
            AdjacencyGraph<SimulationNode, TaggedEdge<SimulationNode, EdgeTag>> graph, SimulationNode other)
        {
            var cost = other.GetType() == typeof (UnavailableSource) ? 9999999 : TransportationCost(other);
            var tag = new EdgeTag { Capacity = long.MaxValue, Cost = cost };
            var edge = new TaggedEdge<SimulationNode, EdgeTag>(other, this, tag);
            graph.AddEdge(edge);

            _outEdges.Add(edge);
            other._inEdges.Add(edge);
        }
    }
}
