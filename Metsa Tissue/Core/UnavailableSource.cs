﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metsa_Tissue.Core
{
    class UnavailableSource : SimulationNode
    {
        private Simulation _simulation;

        public UnavailableSource(Simulation simulation)
        {
            _simulation = simulation;
        }

        public override string DescribeNodeType()
        {
            return "Unavailable Source (internal)";
        }

        public override bool IsInternal()
        {
            return true;
        }

    }
}
