﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metsa_Tissue.Core
{
    public class EdgeTag
    {
        public long Flow;
        public long LoadedFlow;
        public long Capacity;
        public double Cost;

        public string Describe()
        {
            return string.Format("({0}/{1}, {2:F1})", Flow, Capacity == long.MaxValue ? "MAX" : Capacity.ToString(), Cost);
        }
    }
}
